CREATE TABLE IF NOT EXISTS `phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `other_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `duration` varchar(200) NOT NULL,
  `length` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `direction` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
  
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

